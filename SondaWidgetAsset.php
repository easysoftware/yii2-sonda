<?php

namespace easysoftware\sonda;

use yii\web\AssetBundle;

/**
 * Class SondaWidgetAsset
 * @package easysoftware\sonda
 */
class SondaWidgetAsset extends AssetBundle
{
    public function init()
    {
        $this->sourcePath = __DIR__ . '/assets';
        parent::init();
    }

    public $publishOptions = [
        'forceCopy' => YII_ENV_DEV
    ];

    public $css = [
        'css\sonda.css'
    ];

    public $js = [
        'js\sonda.js'
    ];

    public $depends = [
        'yii\web\YiiAsset',
    ];
}