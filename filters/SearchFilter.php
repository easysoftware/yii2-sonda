<?php

namespace easysoftware\sonda\models\search;

use easysoftware\sonda\models\Sonda;
use yii\data\ActiveDataProvider;

/**
 * Class SearchFilter
 * @package easysoftware\sonda\models\search
 */
class SearchFilter extends Sonda
{
    /**
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = parent::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['defaultPageSize' => 10],
            'sort' => ['defaultOrder' => ['survey_created_at' => SORT_DESC]]
        ]);

        $this->load($params);

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        return $dataProvider;
    }
}