<?php

namespace easysoftware\sonda\controllers;

use easysoftware\sonda\forms\AnswerForm;
use easysoftware\sonda\models\SondaAnswers;
use easysoftware\sonda\models\SondaQuestion;
use yii\base\NotSupportedException;
use yii\web\Controller;
use easysoftware\sonda\forms\NewSonda;
use easysoftware\sonda\forms\SondaForm;
use easysoftware\sonda\models\Sonda;
use yii\web\NotFoundHttpException;
use Yii;

/**
 * Class AdminController
 * @package app\modules\sonda\controllers
 */
class AdminController extends Controller
{
    /**
     * @param $action
     * @return bool
     * @throws NotSupportedException
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        $user = \Yii::$app->user->identity;
        if (!$user->hasPrivilege('sonda')) {
            throw new NotSupportedException();
            return false;
        };
        $this->layout = \Yii::$app->params['adminLayout'];
        return parent::beforeAction($action);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index', [
            'sondy' => Sonda::find()
                ->where(['deleted' => 0])
                ->all()
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionNew()
    {
        $formModel = new NewSonda();
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            if ($formModel->load($post) && $formModel->validate()) {
                $sonda = new Sonda();
                $sonda->name = $formModel->name;
                $sonda->tag = $formModel->tag;
                $sonda->category = $formModel->category;
                $sonda->layout_name = $formModel->layout_name;
                $sonda->save();
                return $this->redirect('/sonda/admin');
            }
        }
        return $this->render('new', [
            'formModel' => $formModel
        ]);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionEdit($id)
    {
        if ($sonda = Sonda::findOne(['id' => $id])) {
            $formModel = new SondaForm();
            $formModel->setAttributes(['tag' => $sonda->tag, 'category' => $sonda->category, 'question' => $sonda->questions ? $sonda->questions[0]->question : null, 'layout_name' => $sonda->layout_name]);
            if (Yii::$app->request->isPost) {
                $post = Yii::$app->request->post();

                if ($formModel->load($post) && $formModel->validate()) {
                    $formModel->save($sonda);
                    return $this->redirect('/sonda/admin/edit?id=' . $id);
                }
            }

            return $this->render('edit', [
                'formModel' => $formModel,
                'sonda' => $sonda
            ]);
        }
        throw new NotFoundHttpException();
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionDelete($id)
    {
        $sonda = Sonda::findOne(['id' => $id]);
        $sonda->deleted = 1;
        $sonda->save();
        return $this->redirect('/sonda/admin');
    }

    /**
     * @param $sid
     * @param $qid
     * @return string|\yii\web\Response
     */
    public function actionAddAnswer($sid, $qid)
    {
        $formModel = new AnswerForm();
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            if ($formModel->load($post) && $formModel->validate()) {
                $sonda = new SondaAnswers();
                $sonda->answer = $formModel->answer;
                $sonda->question_id = $qid;
                $sonda->sonda_id = $sid;
                $sonda->save();
                return $this->redirect('/sonda/admin/edit?id=' . $sid);
            }
        }
        return $this->render('add', [
            'formModel' => $formModel
        ]);
    }

    public function actionDeleteAnswer($id)
    {
        $sa = SondaAnswers::findOne(['id' => $id]);
        $sa->deleted = 1;
        $sa->save();
        return $this->redirect('/sonda/admin');
    }

    public function actionDeleteQuestion($id)
    {
        $sq = SondaQuestion::findOne(['id' => $id]);
        $sq->deleted = 1;
        $sq->save();
        return $this->redirect('/sonda/admin');
    }
}
