<?php

namespace easysoftware\sonda\helpers;

use easysoftware\sonda\models\SondaUserAnswers;

/**
 * Class Check
 * @package easysoftware\sonda\helpers
 */
class Check
{
    /**
     * @param string $ip
     * @param int $sondaId
     * @return array|SondaUserAnswers|\yii\db\ActiveRecord|null
     */
    public static function checkIfIpWasUsedInLast24Hrs(string $ip, int $sondaId)
    {
        return SondaUserAnswers::find()
            ->where(['ip' => $ip])
            ->andWhere(['sonda_id' => $sondaId])
            ->andWhere('created_at > CURRENT_TIMESTAMP - INTERVAL 1 DAY')
            ->one();
    }
}
