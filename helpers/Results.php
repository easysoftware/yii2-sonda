<?php

namespace easysoftware\sonda\helpers;

use easysoftware\sonda\models\Sonda;

/**
 * Class Results
 * @package easysoftware\sonda\helpers
 */
class Results
{
    /**
     * @param $id
     * @return array
     */
    public static function getSondaResults($id): array
    {
        $sonda = Sonda::findOne(['id' => $id]);

        $results = [];
        foreach ($sonda->userAnswers as $answer) {
            $results[$answer->sondaAnswer->answer] = isset($results[$answer->sondaAnswer->answer]) ? ++$results[$answer->sondaAnswer->answer] : 1;
        }

        return [
            'results' => $results,
            'sonda' => $sonda,
        ];
    }
}
