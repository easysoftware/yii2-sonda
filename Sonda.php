<?php

namespace easysoftware\sonda;

use easysoftware\sonda\helpers\Check;
use easysoftware\sonda\helpers\Results;
use easysoftware\sonda\models\SondaAnswers;
use easysoftware\sonda\models\SondaUserAnswers;
use yii\web\NotFoundHttpException;
use easysoftware\sonda\models\Sonda as SondaModel;

/**
 * Class Sonda
 * @package easysoftware\sonda
 */
class Sonda extends \yii\base\Widget
{
    public $tag = null;
    public $category = null;

    public function init()
    {
        \Yii::setAlias('@sondaRoot', __DIR__);

        parent::init();
    }

    public function getViewPath()
    {
        return \Yii::getAlias('@sondaRoot/views');
    }

    /**
     * @return string|null
     */
    public function run()
    {
        $view = $this->getView();
        SondaWidgetAsset::register($view);
        $get = \Yii::$app->request->get();
        if (isset($get['s']) && isset($get['a']) && isset($get['q'])) {
            $this->saveAnswer($get);
            return $this->renderResults($get['s']);
        }

        if ($sonda = $this->findModel()) {
            return $this->renderSonda($sonda);
        }
        return null;

    }

    /**
     * @param SondaModel $sonda
     * @return string
     */
    private function renderSonda(SondaModel $sonda)
    {
        return $this->render('widget/default/index', ['sonda' => $sonda]);
    }

    /**
     * @param string $id
     * @return string
     */
    private function renderResults(string $id)
    {
        return $this->render('widget/default/results', ['data' => Results::getSondaResults($id)]);
    }

    /**
     * @return array|bool|SondaModel|\yii\db\ActiveRecord|null
     */
    protected function findModel()
    {
        $query = SondaModel::find();
        if (!empty($this->tag)) {
            $sonda = $query
                ->where(['deleted' => false])
                ->andWhere(['like', 'tag', '%' . $this->tag .'%', false])
                ->one();
        } else {
            $sonda = $query
                ->where(['deleted' => false])
                ->andWhere(['category' => $this->category])
                ->one();
        }
        if ($sonda) {
            $sonda->addPageView();
            return $sonda;
        } else {
            return false;
        }
    }

    /**
     * @param $get
     */
    private function saveAnswer($get)
    {
        $ip = \Yii::$app->request->getUserIP();
        $answer = SondaAnswers::findOne(['sonda_id' => $get['s'], 'question_id' => $get['q'], 'answer' => $get['a']]);
        if ($answer && !Check::checkIfIpWasUsedInLast24Hrs($ip, $get['s'])) {
            $userAnswear = new SondaUserAnswers();
            $userAnswear->sonda_id = $get['s'];
            $userAnswear->question_id = $get['q'];
            $userAnswear->answer_id = $answer->id;
            $userAnswear->value = $get['a'];
            $userAnswear->ip = \Yii::$app->request->getUserIP();
            $userAnswear->save();
            return;
        }
    }
}