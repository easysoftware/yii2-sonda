<?php

namespace easysoftware\sonda\forms;

use easysoftware\sonda\models\Sonda;
use easysoftware\sonda\models\SondaQuestion;
use yii\base\Model;

/**
 * Class ContactForm
 * @package app\forms
 */
class SondaForm extends Model
{
    public $question;
    public $tag;
    public $category;
    public $layout_name;

    public function rules()
    {
        return [
            [['question', 'tag', 'category', 'layout_name'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'question' => 'Pytanie',
            'layout_name' => 'Wygląd sondy'
        ];
    }

    /**
     * @param Sonda $sonda
     */
    public function save(Sonda $sonda)
    {
        if ($this->question) {
            //1 question only
            $q = SondaQuestion::findOne(['sonda_id' => $sonda->id]);
            if (!$q) {
                $q = new SondaQuestion();
                $q->sonda_id = $sonda->id;
            }
            $q->question = $this->question;
            $q->save();
        }
        $this->tag ? $sonda->tag = $this->tag : null;
        $this->category ? $sonda->category = $this->category : null;
        $this->layout_name ? $sonda->layout_name = $this->layout_name : null;
        $sonda->save();
    }
}
