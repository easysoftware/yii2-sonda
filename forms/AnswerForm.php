<?php

namespace easysoftware\sonda\forms;

use yii\base\Model;

/**
 * Class AnswerForm
 * @package easysoftware\sonda\forms
 */
class AnswerForm extends Model
{
    public $answer;

    public function rules()
    {
        return [
            ['answer', 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
        ];
    }
}
