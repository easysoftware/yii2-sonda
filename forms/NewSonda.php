<?php

namespace easysoftware\sonda\forms;

use yii\base\Model;

/**
 * Class NewSonda
 * @package easysoftware\sonda\forms
 */
class NewSonda extends Model
{
    public $name;
    public $tag;
    public $category;
    public $layout_name;

    public function rules()
    {
        return [
            [['name', 'tag', 'category', 'layout_name'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'category' => 'Kateogria',
            'name' => 'Nazwa',
            'tag' => 'Tag',
            'layout_name' => 'Wygląd sondy'
        ];
    }
}
