<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>

<div class="container">
    <h2>Nowa Sonda</h2>
    <div class="col-lg-6">
        <?php $form = ActiveForm::begin([]); ?>
        <?= $form->field($formModel, 'name') ?>
        <?= $form->field($formModel, 'tag') ?>
        <?= $form->field($formModel, 'category') ?>
        <?= $form->field($formModel, 'layout_name')->dropDownList([null => 'standard' , 'uniben' => 'uniben', 'pbkm' => 'pbkm', 'hartmann' => 'hartmann', 'hartmann_ntm' => 'hartmann_ntm']) ?>
        <?= Html::submitButton('Dodaj', ['class' => 'btn btn-success']) ?>
        <?php ActiveForm::end(); ?>

    </div>
</div>