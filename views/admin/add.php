<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>

<div class="container">
    <h2>Nowa Odpowiedz</h2>
    <div class="col-lg-6">
        <?php $form = ActiveForm::begin([]); ?>
        <?= $form->field($formModel, 'answer') ?>
        <?= Html::submitButton('Dodaj', ['class' => 'btn btn-success']) ?>
        <?php ActiveForm::end(); ?>

    </div>
</div>