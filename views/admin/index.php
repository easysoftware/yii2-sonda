<div class="container">
    <h2>Sonda</h2>
    <p class="align-right"><a href="/sonda/admin/new" class="btn btn-success">Nowa sonda</a></p>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Id</th>
            <th>Nazwa</th>
            <th>Tag</th>
            <th>Kategoria</th>
            <th>Usunięta</th>
            <th>Wygląd</th>
            <th>Liczba odsłon</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($sondy as $sonda) : ?>
            <tr>
                <td><?= $sonda->id ?></td>
                <td><?= $sonda->name ?></td>
                <td><?= $sonda->tag ?></td>
                <td><?= $sonda->category ?></td>
                <td><?= $sonda->deleted == 1 ? 'Tak' : 'Nie'; ?></td>
                <td><?= $sonda->layout_name ?></td>
                <td><?= $sonda->page_views ?></td>
                <td>
                    <a href="/sonda/admin/edit?id=<?=$sonda->id?>">Edytuj</a>
                    <a href="/sonda/admin/delete?id=<?=$sonda->id?>">Usuń</a>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
