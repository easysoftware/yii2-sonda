<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>

<div class="container">
    <h2>Sonda</h2>
    <div class="col-lg-6">
        <h2>Lista pytań:</h2>
        <?php foreach ($sonda->questions as $question) : ?>
            <div class="row">
                <h3>pytanie: <?= $question->question ?></h3>
                <?php foreach ($question->answers as $answer) : ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <p><?= $answer->answer?>
                                <a href="/sonda/admin/delete-answer?id=<?= $answer->id ?>">usuń</a>
                                <a href="/sonda/admin/edit-answer?id=<?= $answer->id ?>">edytuj</a>
                            </p>
                        </div>
                    </div>
                <?php endforeach; ?>
                <a href="/sonda/admin/add-answer?sid=<?= $sonda->id?>&qid=<?= $question->id?>" >Dodaj odpowiedź</a>
                <hr>
            </div>
        <?php endforeach; ?>
    </div>
    <div class="col-lg-6">
        <?php $form = ActiveForm::begin([]); ?>
        <?= $form->field($formModel, 'question') ?>
        <?= $form->field($formModel, 'layout_name')->dropDownList([null => 'standard' , 'uniben' => 'uniben', 'pbkm' => 'pbkm', 'hartmann' => 'hartmann', 'hartmann_ntm' => 'hartmann_ntm']) ?>
        <?= Html::submitButton('Edytuj', ['class' => 'btn btn-success']) ?>
        <?php ActiveForm::end(); ?>
    </div>
</div>
