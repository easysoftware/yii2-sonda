<?php
\easysoftware\sonda\SondaWidgetAsset::register($this);
$assets = \app\modules\microsite\assets\MicrositeAsset::register($this);
/** @var \easysoftware\sonda\models\SondaQuestion $question */

$explodedName = explode('_', $sonda->layout_name);
$patron = ($explodedName[0] == 'pbkm'
    ? 'Polski Bank Komórek Macierzystych'
    : strtoupper($explodedName[0]));
?>
<form method="get" action="<?= Yii::$app->request->url?>#panel-sonda">
    <div class="panel panel-default panel-sonda">
        <?php foreach ($sonda->questions as $question) : ?>
            <div class="panel-heading">Pytanie: <?= $question->question ?></div>
            <div class="panel-body">

                <?php if ($sonda->layout_name !== '') : ?>
                <div class="col-md-8">
                    <?php endif; ?>

                    <?php foreach ($question->answers as $answer) : ?>
                        <p>
                            <input type="radio" name="a" value="<?= $answer->answer ?>"> &nbsp;&nbsp;<?= $answer->answer ?>
                        </p>
                    <?php endforeach; ?>

                    <?php if ($sonda->layout_name !== '') : ?>
                </div>
                <div class="col-md-4">
                    <img src="<?= $assets->baseUrl?>/img/sonda/<?= $sonda->layout_name ?>_logo.png" style="width: 100px; margin-left: 40px;">
                    <p class="text-center">Patronem sondy jest <b><?= $patron ?></b></p>
                </div>
            <?php endif; ?>

            </div>
            <input type="hidden" name="q" value="<?= $question->id ?>">
        <?php endforeach; ?>
        <div class="panel-body">
            <input type="hidden" name="s" value="<?= $sonda->id ?>">
            <input class="btn btn-lg btn-active" type="submit" name="odp" value="Odpowiedz">
        </div>
        <?php if ($sonda->layout_name !== '') : ?>
            <img class="sonda-under-button" src="<?= $assets->baseUrl?>/img/sonda/<?= $sonda->layout_name ?>.jpg">
        <?php endif; ?>
    </div>
</form>
