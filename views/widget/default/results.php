<?php
/** @var \easysoftware\sonda\models\Sonda $sonda */
$sonda = $data['sonda'];
$results = $data['results'];
$assets = \app\modules\microsite\assets\MicrositeAsset::register($this)
?>
<div id="panel-sonda" style="height: 80px"></div>
<div class="panel panel-default panel-sonda">
    <div class="panel-heading">
        <?= $sonda->questions[0]->question; ?>
    </div>
    <div class="panel-body">
        <?php foreach ($results as $question => $result) : ?>
            <div class="col-lg-3">
                <p style="font-size:small;overflow:hidden;white-space:nowrap;text-overflow:ellipsis;"><?= $question ?></p>
            </div>
            <div class="col-lg-9">
                <div class="progress">
                    <div class="progress-bar medme__horizontal-gradient" role="progressbar"
                         aria-valuenow="<?= $sonda->userAnswers == 0 ? 0 : $result / count($sonda->userAnswers) * 100 ?>"
                         aria-valuemin="0" aria-valuemax="100"
                         style="width:<?= $sonda->userAnswers == 0 ? 0 : $result / count($sonda->userAnswers) * 100 ?>%">
                        <?= $sonda->userAnswers == 0 ? 0 : number_format($result / count($sonda->userAnswers) * 100,
                            2) ?>%
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    <?php if ($sonda->layout_name !== '') : ?>
        <img class="sonda-under-button" src="<?= $assets->baseUrl?>/img/sonda/<?= $sonda->layout_name ?>.jpg">
    <?php endif; ?>
</div>
