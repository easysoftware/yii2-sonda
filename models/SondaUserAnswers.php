<?php

namespace easysoftware\sonda\models;

/**
 * Model class for table "sonda_user_answer".
 *
 * @property integer $id
 * @property integer $sonda_id
 * @property integer $question_id
 * @property string $answer_id
 * @property string $value
 * @property string $ip
 * @property string $created_at
 *
 * @property SondaAnswers $sondaAnswer
 * @property SondaQuestion $question
 * @property Sonda $Survey
 */
class SondaUserAnswers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'sonda_id', 'question_id', 'answer_id', 'value'], 'safe'],
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSondaAnswer()
    {
        return $this->hasOne(SondaAnswers::class, ['id' => 'answer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestion()
    {
        return $this->hasOne(SondaQuestion::class, ['id' => 'question_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSurveyUserAnswerSurvey()
    {
        return $this->hasOne(Sonda::class, ['id' => 'sonda_id']);
    }
}