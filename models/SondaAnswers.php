<?php

namespace easysoftware\sonda\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "sonda_answers".
 *
 * @property int $id
 * @property int $sonda_id
 * @property int $question_id
 * @property string $answer
 * @property bool $deleted
 *
 * @property SondaQuestion $question
 */
class SondaAnswers extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sonda_answers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'sonda_id', 'question_id', 'answer', 'deleted'], 'safe'],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestion()
    {
        return $this->hasOne(SondaQuestion::class, ['id' => 'question_id']);
    }
}
