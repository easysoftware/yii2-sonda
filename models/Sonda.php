<?php

namespace easysoftware\sonda\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "sonda".
 * @property int $id
 * @property string $name
 * @property bool $deleted
 * @property string $tag
 * @property string $category
 * @property string $layout_name
 * @property int $page_views
 *
 * @property SondaQuestion[] $questions
 * @property SondaAnswers[] $answers
 * @property SondaUserAnswers[] $userAnswers
 */
class Sonda extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sonda';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name', 'tag', 'category', 'deleted', 'layout_name', 'page_views'], 'safe'],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSondaAnswers()
    {
        return $this->hasMany(SondaAnswers::class, ['sonda_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestions()
    {
        return $this->hasMany(SondaQuestion::class, ['sonda_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserAnswers()
    {
        return $this->hasMany(SondaUserAnswers::class, ['sonda_id' => 'id']);
    }

    /**
     * add PageView.
     */
    public function addPageView()
    {
        $this->page_views++;
        $this->save();
    }
}
