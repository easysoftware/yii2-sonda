<?php

namespace easysoftware\sonda\models;

use yii\db\ActiveRecord;

/**
 * Class SondaQuestion
 * @package easysoftware\sonda\models
 *
 * @property int $id
 * @property int $sonda_id
 * @property string $question
 * @property bool $deleted
 *
 * @property SondaAnswers[] $answers
 * @property Sonda $sonda
 */
class SondaQuestion extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'sonda_id', 'question', 'deleted'], 'safe'],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSurvey()
    {
        return $this->hasOne(Sonda::class, ['id' => 'sonda_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnswers()
    {
        return $this->hasMany(SondaAnswers::class, ['question_id' => 'id'])->orderBy(['id' => SORT_ASC]);
    }
}
