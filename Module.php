<?php

namespace easysoftware\sonda;

/**
 * Class Module
 * @package easysosftware\sonda
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->controllerNamespace = 'easysoftware\sonda\controllers';

        parent::init();
    }
}
