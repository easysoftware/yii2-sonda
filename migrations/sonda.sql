CREATE TABLE `sonda` (
  `id` int(10) UNSIGNED NOT NULL
);

ALTER TABLE `sonda`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `sonda`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

CREATE TABLE `sonda_answers` ( `id` INT(10) UNSIGNED NOT NULL , `sonda_id` INT(10) UNSIGNED NOT NULL ,
 `question_id` INT(10) UNSIGNED NOT NULL , `answer` VARCHAR(255) NOT NULL , PRIMARY KEY (`id`), INDEX (`sonda_id`), INDEX (`question_id`));

CREATE TABLE `sonda_question` ( `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
 `sonda_id` INT(10) UNSIGNED NOT NULL , `question` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL , PRIMARY KEY (`id`), INDEX (`sonda_id`)) ;

ALTER TABLE `sonda_answers` CHANGE `id` `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT;

CREATE TABLE `sonda_user_answers` (
  `id` int(10) UNSIGNED NOT NULL,
  `sonda_id` int(10) UNSIGNED NOT NULL,
  `question_id` int(10) UNSIGNED NOT NULL,
  `answer_id` int(10) UNSIGNED NOT NULL,
  `value` varchar (255) NOT NULL
) ;

ALTER TABLE `sonda_user_answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sonda_id` (`sonda_id`),
  ADD KEY `question_id` (`question_id`),
  ADD KEY `answer_id` (`answer_id`);

ALTER TABLE `sonda_user_answers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `sonda` ADD `name` VARCHAR(255) NOT NULL AFTER `id`;
ALTER TABLE `sonda` ADD `tag` VARCHAR(255) NULL AFTER `id`;
ALTER TABLE `sonda` ADD `category` VARCHAR(255) NULL AFTER `id`;
ALTER TABLE `sonda_answers` ADD `deleted` BOOLEAN NOT NULL AFTER `answer`;
ALTER TABLE `sonda` ADD `deleted` BOOLEAN NOT NULL AFTER `name`;
ALTER TABLE `sonda_question` ADD `deleted` BOOLEAN NOT NULL;

ALTER TABLE `sonda_user_answers` ADD `ip` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '127.0.0.1' AFTER `value`, ADD `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `ip`;
